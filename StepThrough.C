bool nextev = false;
bool quit = false;

void StepThrough(const std::string& filename="second.root")
{
  TFile*     file = TFile::Open(filename.c_str(), "READ");
  TTree*     tree = static_cast<TTree*>(file->Get("T"));
  TH2*       hist = 0;
  tree->SetBranchAddress("event",&hist);

  TCanvas* c = new TCanvas("c","c",800,800);
  c->Draw();

  
  for (size_t i = 0; i < tree->GetEntries(); i++) {
    std::cout << "Event # " << i << std::endl;
    tree->GetEntry(i);

    c->cd();
    hist->Draw("colz");
    TButton* b = new TButton("Next", "nextev=true;",0,.9,.3,1);
    b->Draw();
    TButton* q = new TButton("Quit", "quit=true;",.3,.9,.6,1);
    q->Draw();
    
    c->Modified();
    c->Update();
    c->cd();
    nextev = false;
    quit   = false;
    while (!nextev and !quit) gSystem->ProcessEvents();
    if (quit) break;
      
  }
  c->Delete();
}
