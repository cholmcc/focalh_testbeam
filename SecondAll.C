#include "Second.C"
#include <TSystemDirectory.h>

void SecondAll()
{
  TSystemDirectory dir(".",".");
  TIter            next(dir.GetListOfFiles());
  TSystemFile*     file;
  while ((file = static_cast<TSystemFile*>(next()))) {
    if (!file->IsDirectory()) continue;
    if (file->GetName()[0] == '.') continue;
  
    TSystemDirectory* sub = static_cast<TSystemDirectory*>(file);
    TList* subfiles = sub->GetListOfFiles();

    if (!subfiles->FindObject("merged.root")) continue;

    std::string coordfile = "Coordinates.dat";
    std::string pedfile   = "Pedestals.dat";
    
    if (subfiles->FindObject(coordfile.c_str()))
      coordfile = Form("%s/%s",sub->GetName(), coordfile.c_str());

    if (subfiles->FindObject(pedfile.c_str()))
      pedfile = Form("%s/%s",sub->GetName(), pedfile.c_str());

    std::cout << sub->GetName() << ": " << std::flush;
    Second(Form("%s/merged.root",sub->GetName()),
	   Form("%s/second.root",sub->GetName()),
	   coordfile,
	   pedfile);
  }
}

