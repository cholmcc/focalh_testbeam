TH1* AdcSpectra(TTree* tree, const char* name,
		Color_t color, const char* sel="")
{
  TH1* h = new TH1D(name, name, 100, 0, 200000);
  h->SetXTitle("#sum_{cards} ADC");
  h->SetYTitle("Events");
  h->SetFillColor(color);
  h->SetFillStyle(3001);
  h->SetLineColor(color);

  tree->Draw(Form("Sum$(feb00.adc)+Sum$(feb01.adc)>>%s",name),sel);
  h->SetDirectory(0);

  return h;
}

TH2* Correl(TTree* tree, const char* name, Color_t color, const char* sel="")
{
  TH2* h = new TH2D(name, name, 40, 0, 50000, 40, 0, 100000);
  h->SetXTitle("#sum_{FEB 0} ADC");
  h->SetYTitle("#sum_{FEB 1} ADC");
  h->SetZTitle("Events");
  h->SetFillColor(color);
  h->SetFillStyle(3001);
  h->SetLineColor(color);
  
  tree->Draw(Form("Sum$(feb01.adc):Sum$(feb00.adc)>>%s",name),sel);
  h->SetDirectory(0);

  return h;
}
  
void DrawIt(const char* dirname)
{
  gStyle->SetOptStat(0);
  
  TFile* file = TFile::Open(Form("%s/merged.root",dirname),"READ");
  TTree* tree = static_cast<TTree*>(file->Get("T"));

  TCanvas* c = new TCanvas("c","c", 1000, 600);
  c->Divide(2,1);

  TVirtualPad* p1 = c->cd(1);
  p1->SetLogy();
  p1->SetTopMargin(0.03);
  p1->SetRightMargin(0.01);
  TH1* sumAll = AdcSpectra(tree, "SumAll",  kRed+1,  "");
  TH1* sum2   = AdcSpectra(tree, "SumBoth", kBlue+1, "header.n==2");
  sumAll->Draw();
  sum2  ->Draw("same");
  p1->BuildLegend(0.8,0.8,.99,.97);
  

  TVirtualPad* p2 = c->cd(2);
  p2->SetLogz();
  p2->SetTopMargin(0.03);
  p2->SetLeftMargin(0.15);
  TH2* corrAll = Correl(tree, "CorrAll",  kRed+1,  "");
  TH2* corr2   = Correl(tree, "CorrBoth", kBlack, "header.n==2");
  corr2->SetFillStyle(1);
  corrAll->Draw("colz");
  corr2  ->Draw("box same");
  p2->BuildLegend(0.7,0.8,.9,.97);
}


  
  

  
  
