#include <iostream>
#include <TTree.h>
#include <map>
#include <string>
#include <forward_list>
#include <TFile.h>
#include <TH2.h>
#include <fstream>


//====================================================================
using CoordMap=std::map<UInt_t,std::pair<Short_t,Short_t>>;
using PedMap=std::map<UInt_t,Float_t>;

//====================================================================
UInt_t Encode(UShort_t board, UShort_t ch)
{
  return (board & 0xFF) << 5 | (ch & 0x1F);
}
std::pair<UShort_t,UShort_t> Decode(UInt_t id)
{
  return std::make_pair((id >> 5) & 0xFF, id & 0x1F);
}

//====================================================================
void ReadCoordinates(const std::string& filename,
		     CoordMap& coords)
{
  std::ifstream file(filename.c_str());
  if (!file) {
    std::cerr << "Failed to open coordinate file " << filename << std::endl;
    return;
  }

  std::string line;
  std::getline(file, line); // Read in the comment line

  while (true) {
    if (file.eof()) break;

    UShort_t feb, ch, x, y;
    file >> feb >> ch >> x >> y;

    coords[Encode(feb,ch)] = std::make_pair(x,y);
  }
}

//====================================================================
void ReadPedestals(const std::string& filename,
		   PedMap& coords)
{
  std::ifstream file(filename.c_str());
  if (!file) {
    std::cerr << "Failed to open coordinate file " << filename << std::endl;
    return;
  }

  std::string line;
  std::getline(file, line); // Read in the comment line

  while (true) {
    if (file.eof()) break;

    UShort_t feb, ch;
    Float_t  ped;
    file >> feb >> ch >> ped;

    coords[Encode(feb,ch)] = ped;
  }
}
		   
//====================================================================
/** 
 * Input event structure 
 */
struct Event {
  UInt_t   t[2];
  UInt_t   tref[2];
  UShort_t adc[32];
  UChar_t  board;

  void Clear() {
    t[0] = t[1] = tref[0] = tref[1] = 0;
    board = 0xff;
    for (size_t i = 0; i < 32; i++) adc[i] = 0;
  }
};
struct Header {
  UInt_t t[2];
  UShort_t n;
};

//====================================================================
void Second(const std::string& filename,
	    const std::string& outname="second.root",
	    const std::string& coordfile="Coordinates.dat",
	    const std::string& pedfile="Pedestals.dat")
{
  TFile*     file = TFile::Open(filename.c_str(), "READ");
  if (!file) return;
  
  TTree*     tree = static_cast<TTree*>(file->Get("T"));
  TObjArray* brns = tree->GetListOfBranches();
  Header     hdr;
  std::forward_list<Event*> events;
  for (auto o : *brns) {
    if (std::string(o->GetName()) == "header") continue;
    
    auto ev = new Event;
    events.push_front(ev);
    tree->SetBranchAddress(o->GetName(), ev);
  }
    
  tree->SetBranchAddress("header",&hdr);
  
  
  CoordMap coords;
  PedMap   peds;

  ReadCoordinates(coordfile, coords);
  ReadPedestals  (pedfile,   peds);

  Short_t xmin = 0xFFFF, xmax = -1;
  Short_t ymin = 0xFFFF, ymax = -1;
  for (auto c : coords) {
    xmin = std::min(c.second.first,xmin); 
    xmax = std::max(c.second.first,xmax);
    ymin = std::min(c.second.first,ymin); 
    ymax = std::max(c.second.first,ymax);
  }
  // std::cout << xmin << "\t" << xmax << "\t"
  // 	    << ymin << "\t" << ymax << "\t" << std::endl;

  TH2* h = new TH2D("event","event",
		    (xmax-xmin)+1,xmin-.5,xmax+.5,
		    (ymax-ymin)+1,ymin-.5,ymax+.5);
  h->SetDirectory(0);
  
    
  TFile* out   = TFile::Open(outname.c_str(), "RECREATE");
  TTree* otree = new TTree("T","T");
  otree->Branch("event",&h);

  TH2* p = static_cast<TH2*>(h->Clone("pedestals"));
  for (auto ip : peds) {
    auto xy = Decode(ip.first);
    p->Fill(xy.first, xy.second, ip.second);
  }
  
  size_t nev = 0;
  for (Long64_t i = 0; i < tree->GetEntries(); i++) {
    tree->GetEntry(i);
    

    if (hdr.n != 2) continue;
    nev++;
    h->Reset();
    
    for (auto e : events) {
      UShort_t feb = e->board;

      for (size_t ch = 0; ch < 32; ch++) {
	UInt_t id = Encode(feb,ch);
	auto coord = coords[id];
	auto ped   = peds[id];
	auto sig   = std::max(e->adc[ch] - ped,0.F);
	h->Fill(coord.first, coord.second, sig);
      }
    }
    otree->Fill();
  }
  std::cout << "Got " << nev << " out of "
	    << tree->GetEntries() << " events" << std::endl;


  out->Close();
    
}


  
