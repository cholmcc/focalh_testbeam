# Software to analyse FoCAL-H test beam data 

## Code 

- [`Collate.C`](Collate.C) Collate events from any number of boards
  into a events.  The output is a new file with a `TTee`.  This tree
  has the branches
  
  - `header` 
    - `t[2]` Mean time of the event 
	- `n` Number of boards in the event 
  - `feb`_XX_ (_XX_ in hex) 
    - `board` Board ID 
	- `adc[32]` ADC values of 32 channels 
	- `t` Timers 
	- `tref` Reference time (always zero) 
	
  There's one `feb`_XX_ branch for each attached card.  A card _not_
  participating in an event has `board=0xFF`. 
  
  To change the threshold for merging events, pass it as first
  argument 
  
      root -l Collate.C\(100\)
  
- [`CollateAll.C`](CollateAll.C) Collate all runs in sub-directory of
  the current directory.  Each output is written into the same
  sub-directory as `merged.root` 

  To change the threshold for merging events, pass it as first
  argument 
  
      root -l CollateAll.C\(100\)

- [`DrawSpectra.C`](DrawSpectra.C) Draws all ADC spectra from merged
  files. 
  
- [`Second.C`](Second.C) reads in merged event data as well as
  pedestal and coordinates and produces a new `TTree`. Only events
  with more than one card is accepted.  For such events, the pedestal
  subtracted ADC values are written to a `TH2` where the axes are the
  coordinates.  That is, we have one histogram per event in the output
  tree. 
  
- [`SecondAll.C`](SecondAll.C) Does the second level analysis
  (`Second.C`) on all merged event files in sub-directories of the
  current directory.  The output is stored in `second.root` in the
  relevant sub-directory.  
  
- [`StepThrough.C`](StepThrough.C) reads `second.root` as produced by
  the above and displays each event at a time.  For example 
  
      root -l StepThrough.C\(\"2021_10_05_22_43_26/second.root\"\)
	  
  ![event 8](event08.png)
  ![event 14](event14.png)
  
  
## Other files 

These are used by `Second.C` 

- [`Pedestals.dat`](Pedestals.dat) Simple space separated database of
  pedestals. Columns are
  
  - _board_
  - _channel_ 
  - _pedestal_ 
  
- [`Coordinates.dat`](Coordinates.dat) Simple space separated database
  of coordinates.  Columns are 
  
  - _board_
  - _channel_
  - _x_
  - _y_
  

